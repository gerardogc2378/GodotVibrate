# GodotVibrate for Android
- Vibration support on Godot engine for Android. (https://godotengine.org)

- Godot version 2.1.x.

## How to use
- Drop vibrate folder into godot/modules

- Copy vibrate/GodotVibrate.java file to godot/platform/android/java/src/org/godotengine/godot

**Note:** The vibrate/android.jar is taken from  *android-sdk-linux/platforms/android-28*. You may choose to use any other api version.

### Compile
- scons platform=android target=release
- cd godot/platform/android/java
- ./gradlew build
- The resulting APKs will be available at godot/bin/*.apk

**Note:** This module was tested with NDK 17.1.4828580 version. Please check how to build your new APKs from here: (https://godotengine.org/article/fixing-godot-games-published-google-play)

### Configure
Add the following in the engine.cfg file:

```

[android]

modules="org/godotengine/godot/GodotVibrate"

```

Using in gdscript:

```

var vibrate = null

vibrate = Globals.get_singleton("GodotVibrate")

vibrate.doVibrate(200) # milliseconds

```

### Build with the new APKs
From Godot Editor:

> Export -> Target -> Android


Custom Package (Debug/Release):
> Point to your new APKs

> Permission check: Vibrate

#### License
MIT
