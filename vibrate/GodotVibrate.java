package org.godotengine.godot;

//imports
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;

public class GodotVibrate extends Godot.SingletonBase {

  private Godot activity;

  public void doVibrate(int ms) {
    Vibrator v = (Vibrator) activity.getSystemService(Context.VIBRATOR_SERVICE);
    if (v.hasVibrator()) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        VibrationEffect effect = VibrationEffect.createOneShot(ms, 255);
        v.vibrate(effect);
      }
      else {
        //deprecated in API 26
        v.vibrate(ms);
      }
    }
  }

  static public Godot.SingletonBase initialize(Activity p_activity) {
    return new GodotVibrate(p_activity);
  }

  public GodotVibrate(Activity p_activity) {
    registerClass("GodotVibrate", new String[]{"doVibrate"});
    activity = (Godot)p_activity;
  }
}
